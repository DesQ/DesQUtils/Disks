# DesQ Disks
## Disks Auto Mounter for DesQ Desktop Environment

This is a simple disks auto-mounter based on udisks/dbus.


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Disks.git DesQDisks`
- Enter the `DesQDisks` folder
  * `cd DesQDisks`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* [libdesq](https://gitlab.com/DesQ/libdesq)
* [libdesqui](https://gitlab.com/DesQ/libdesqui)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::Xdg](https://gitlab.com/desktop-frameworks/xdg)
* [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Storage](https://gitlab.com/desktop-frameworks/storage)


### Known Bugs
* Devices are not shown after first-time start after a reboot.


### Upcoming
* Notification with actions to mount the drive (if AutoMount is false)
