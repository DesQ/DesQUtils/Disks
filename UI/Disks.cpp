/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Disks.hpp"
#include "DriveWidget.hpp"

#include <DFStorage.hpp>

DesQ::Disks::UI::UI() : QScrollArea() {
    QShortcut *dismiss   = new QShortcut( QKeySequence( Qt::Key_Escape ), this );
    QShortcut *reloadAct = new QShortcut( QKeySequence( Qt::Key_F5 ), this );

    connect( dismiss,   &QShortcut::activated, this, &DesQ::Disks::UI::close );
    connect( reloadAct, &QShortcut::activated, this, &DesQ::Disks::UI::reload );

    reloadDelayer = new QBasicTimer();

    connect(
        diskMgr, &DFL::Storage::Manager::reload, [ = ] () {
            if ( reloadDelayer->isActive() ) {
                reloadDelayer->stop();
                reloadDelayer->start( 250, this );
            }

            else {
                reloadDelayer->start( 250, this );
            }
        }
    );

    buildUI();
    setWindowProperties();
}


void DesQ::Disks::UI::startTray() {
    QSystemTrayIcon *icon = new QSystemTrayIcon( QIcon::fromTheme( "desq-disks", QIcon( ":/icons/desq-disks.png" ) ), this );

    icon->show();

    connect(
        icon, &QSystemTrayIcon::activated, [ = ]( QSystemTrayIcon::ActivationReason reason ) {
            switch ( reason ) {
                case QSystemTrayIcon::Trigger: {
                    if ( isVisible() ) {
                        hide();
                    }

                    else {
                        show();
                    }

                    break;
                }

                case QSystemTrayIcon::Context: {
                    break;
                }

                case QSystemTrayIcon::MiddleClick: {
                    qApp->quit();
                    break;
                }

                case QSystemTrayIcon::DoubleClick: {
                    break;
                }

                case QSystemTrayIcon::Unknown: {
                    break;
                }
            }
        }
    );
}


void DesQ::Disks::UI::handleDeviceAdded( QString ) {
    reload();
}


void DesQ::Disks::UI::handleDeviceRemoved( QString ) {
    reload();
}


void DesQ::Disks::UI::handlePartitionAdded( QString ) {
    reload();
}


void DesQ::Disks::UI::handlePartitionRemoved( QString ) {
    reload();
}


void DesQ::Disks::UI::buildUI() {
    baseLyt = new QVBoxLayout();

    for ( DFL::Storage::Device *device: diskMgr->devices() ) {
        QString            devName( QFileInfo( device->path() ).baseName() );
        DesQ::Disks::Drive *drive = new DesQ::Disks::Drive( device, nullptr );
        baseLyt->addWidget( drive );

        drives[ devName ] = drive;
        for ( DFL::Storage::Volume *blk: device->allVolumes() ) {
            parts[ QFileInfo( blk->devicePath() ).baseName() ] = devName;
        }
    }
    baseLyt->addStretch();

    QWidget *base = new QWidget();

    base->setLayout( baseLyt );
    setWidget( base );
}


void DesQ::Disks::UI::setWindowProperties() {
    setWindowIcon( QIcon( ":/icons/desq-disks.png" ) );
    setWindowTitle( "DesQ Disks Manager" );

    setMinimumSize( 400, 300 );
    setWidgetResizable( true );
    setAlignment( Qt::AlignCenter );

    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
}


void DesQ::Disks::UI::reload() {
    /** Remove added drives */
    for ( QString dev: drives.keys() ) {
        DesQ::Disks::Drive *drive = drives.take( dev );
        delete drive;
    }

    /** Clear the partition-drive map */
    parts.clear();

    for ( DFL::Storage::Device *device: diskMgr->devices() ) {
        QString            devName( QFileInfo( device->path() ).baseName() );
        DesQ::Disks::Drive *drive = new DesQ::Disks::Drive( device, nullptr );
        baseLyt->insertWidget( baseLyt->count() - 1, drive );

        drives[ devName ] = drive;
        for ( DFL::Storage::Volume *blk: device->allVolumes() ) {
            parts[ QFileInfo( blk->devicePath() ).baseName() ] = { devName };
        }
    }
}


void DesQ::Disks::UI::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();
    widget()->setFixedWidth( viewport()->width() );
}


void DesQ::Disks::UI::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == reloadDelayer->timerId() ) {
        reloadDelayer->stop();
        reload();

        return;
    }

    QScrollArea::timerEvent( tEvent );
}
