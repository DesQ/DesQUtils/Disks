/**
 * This file is a part of DesQ Disks.
 * DesQ Disks is the Disks Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <DFStorage.hpp>

namespace DesQ {
    namespace Disks {
        class Partition;
        class Drive;
    }
}

/* UI to handle a partition */
class DesQ::Disks::Partition : public QWidget {
    Q_OBJECT;

    public:
        Partition( DFL::Storage::Volume *vol, QWidget * );
        ~Partition();

    private:
        DFL::Storage::Volume *mBlock;
        QToolButton *mtBtn;
        QToolButton *openBtn;
        QProgressBar *usage;

        bool mPressed;

        void mountUnmount();
};

/* UI to handle a disk/usb drive */
class DesQ::Disks::Drive : public QWidget {
    Q_OBJECT;

    public:
        Drive( DFL::Storage::Device *drive, QWidget * );
        ~Drive();

        void update();

    private:
        QVBoxLayout *baseLyt;
        QLabel *label;
        QList<DesQ::Disks::Partition *> parts;
        DFL::Storage::Device *stDev;

        bool mFolded;

    protected:
        void mouseReleaseEvent( QMouseEvent * ) override;
        void paintEvent( QPaintEvent *pEvent ) override;
};
